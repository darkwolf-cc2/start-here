FROM fedora:latest as binaries
RUN curl  https://releases.hashicorp.com/vault/1.7.0-rc1/vault_1.7.0-rc1_linux_amd64.zip -o vault_1.7.0-rc1_linux_amd64.zip && \
    gunzip -S zip vault_1.7.0-rc1_linux_amd64.zip && \
    mv vault_1.7.0-rc1_linux_amd64. vault && \
    chmod +x vault

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x kubectl


FROM quay.io/operator-framework/ansible-operator:v1.5.0

COPY --from=binaries /vault /usr/bin/vault
COPY --from=binaries /kubectl /usr/bin/kubectl
COPY requirements.yml ${HOME}/requirements.yml
RUN ansible-galaxy collection install -r ${HOME}/requirements.yml \
 && chmod -R ug+rwx ${HOME}/.ansible \
 && pip3 install --user psycopg2-binary PyMySQL PyMySQL[rsa] PyMySQL[ed25519] 


COPY watches.yaml ${HOME}/watches.yaml
COPY roles/ ${HOME}/roles/
COPY playbooks/ ${HOME}/playbooks/
