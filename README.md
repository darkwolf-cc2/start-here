# The "Start Here" Key Rotation Operator

## Description

The "Start Here" Operator works to take the work out of key rotations. Instead of logging into different systems and updating `secrets` manually, a user only has to create a `kind: rotation` resource using `kubectl` with the information about what
credentials to update and the "Start Here" Operator will generate a new password, update the Vault secret, update the Postgresql password on the database, and initialize a new rollout on deployments with matching annotations. 
This process currently works with Postgresql databases, but because the operator is written in Ansible, it is readily extensible to any type of service that can be managed by Ansible, from Redis to switches.

## Operation Details

The Operator works in five main steps:

1. `Rotation` Resource Creation

    The `rotation` resource is created using the usual resource creation methods in Kubernetes (e.g.: kubectl). 
    For an example of what this resource looks like, see the [config/samples/encryption_v1_rotation.yaml](https://gitlab.com/darkwolf-cc2/start-here/-/blob/master/config/samples/encryption_v1_rotation.yaml) sample file.
    It should contain 3 keys in the spec:
        1. client_config - The path to the Vault secret that stores the client config to be rotated
        1. master_config - The path to the Vault secret that stores the master configuration that the operator will use to update the client configuration password
        1. vault_url - The path to Vault

1. Retrieving Current Credentials

    The operator first gets a Vault client token by requesting the role `start-here` using the Kubernetes auth module. The `default` service account in the `start-here-system` namespace must be allow to access this roll in the Vault setup (see development instructions below). The `start-here` role must have permissions to "list" and "read" the credentials indicated by the `master_config`, as well as "list", "read", "create" and "update" the credentials indicated by the `client_config`.

1. Credentials Updated in Vault

    The operator will generate a new password using the Ansible lookup `lookup('password', '/dev/null chars=ascii_letters,digits length=16')`. This password will be stored back in Vault under the client_config without changing the db_url, db_name, or username parameters

1. Credentials Update in Target Service

    The operator will now use the username and password indicated in the `master_config` to connect to the database indicated in the `master_config`. It will update the password of the user indicated in the `client_config` to use the new password now stored in Vault under the `client_config` key. This process is done without modifying any of the other account parameters, such as accesses, roles, or account expiration dates.

1. Deployment Rollout Initiated

    In order to gracefully tell different client applications that they should restart because their credentials have been rotated, the operator uses an annotation (`rotation.darkwolf.io/secret: <client_config>`). The operator will search all namespaces for this annotation and initiate rollouts. The value of this annotation must match the value provided in the `client_config` of the resource definition. After enumerating all of the deployments that are tagged as consuming this credential, a rollout is initiated using `kubectl rollout restart deployment <deployment-name> -n <namespace>`.

## Quick Demo

[![asciicast](https://asciinema.org/a/tMip9EH160uXA47Ei1CBKAOrI.svg)](https://asciinema.org/a/tMip9EH160uXA47Ei1CBKAOrI)


### A note on prerequisites
***This demo does not cover setting up your Kubernetes cluster, helm, or initializing a production-ish Vault***  
Refer to the [Development](#development) section below for details on setting up Vault in dev server mode on your own k8s installation.  
This demo runs Hashicorp Vault in ["dev" server mode](https://www.vaultproject.io/docs/concepts/dev-server).  
Note that Vault "dev" server mode runs an *in memory* database and your Vault configuration and secrets will be lost if the Vault Pod restarts.  

### Prepare Helm
1. Add the postgres helm chart:
    ```
    $ helm repo add bitnami https://charts.bitnami.com/bitnami
    ```

### Demo 
1. After git-cloning this repo, you can apply the operator: 
    ```
    $ kubectl apply -k config/default
    ```

1. Create a namespace to work in for the demo and set it as default:
    ```
    $ export DEMO_NAMESPACE="demo-ns-$(date '+%F')"
    $ kubectl create namespace $DEMO_NAMESPACE
    $ kubectl config set-context --current --namespace=$DEMO_NAMESPACE
    ```

1. Deploy a Postgres database to your namespace:
    ```
    helm install demo-psql bitnami/postgresql
    ```

1. Add master password to env:
    ```
    export POSTGRES_PASSWORD=$(kubectl get secret --namespace $DEMO_NAMESPACE demo-psql-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
    ```

1. Create demo table in the database:
    ```
    kubectl run demo-psql-postgresql-client --rm  -it --restart='Never' --namespace $DEMO_NAMESPACE --image docker.io/bitnami/postgresql:11.11.0-debian-10-r31 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host demo-psql-postgresql -U postgres -d postgres -p 5432 -c "CREATE DATABASE demo;"
    ```

1. Add master configuration to vault:
    ```
    kubectl exec -it vault-0 -n default -- vault kv put secret/demo-master username=postgres password=${POSTGRES_PASSWORD} db_host=demo-psql-postgresql.$DEMO_NAMESPACE.svc.cluster.local
    ```

1. Add client configuration to vault:
    ```
    kubectl exec -it vault-0 -n default -- vault kv put secret/demo-config username=democlient password="SuperSecret" db_name=demo db_host=demo-psql-postgresql.$DEMO_NAMESPACE.svc.cluster.local
    ```

1. Demonstrate temp password doesn't work:
    ```
    kubectl run demo-psql-postgresql-client --rm  -i --restart='Never' --namespace $DEMO_NAMESPACE --image docker.io/bitnami/postgresql:11.11.0-debian-10-r31 --env="PGPASSWORD=SuperSecret" --command -- psql --host demo-psql-postgresql -U democlient -d postgres -p 5432
    ```

1. Create new resource:
    ```
    cat <<EOF | kubectl apply -f -
    apiVersion: encryption.darkwolfsolutions.com/v1
    kind: Rotation
    metadata:
        name: rotation-demo
    spec:
        client_config: secret/demo-config
        master_config: secret/demo-master
        vault_url: "http://vault-internal.default.svc.cluster.local:8200"
    EOF
    ```

1. Confirm that the password has change from what we set in the set up:
    ```
    kubectl exec -it vault-0 -n default -- vault kv get -format json secret/demo-config | jq ".data.data.password" -rj
    ```

1. Save the password we just saw:
    ```
    export POSTGRES_PASSWORD=$(kubectl exec -it vault-0 -n default -- vault kv get -format json secret/demo-config | jq ".data.data.password" -rj)
    ```

1. Attempt to connect to Postgres using new password (You might have to press enter, but you'll get a `postgres=>` prompt):
    ```
    kubectl run demo-psql-postgresql-client --rm  -it --restart='Never' --namespace $DEMO_NAMESPACE --image docker.io/bitnami/postgresql:11.11.0-debian-10-r31 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host demo-psql-postgresql -U democlient -d postgres -p 5432
    ```

1. Rotate it again:
    ```
    cat <<EOF | kubectl apply -f -
    apiVersion: encryption.darkwolfsolutions.com/v1
    kind: Rotation
    metadata:
        name: rotate-me-right-round-baby
    spec:
        client_config: secret/demo-config
        master_config: secret/demo-master
        vault_url: "http://vault-internal.default.svc.cluster.local:8200"
    EOF
    ```

1. Show that the new and old passwords are different:
    ```
    echo $POSTGRES_PASSWORD
    kubectl exec -it vault-0 -n default -- vault kv get -format json secret/demo-config | jq ".data.data.password" -rj
    ```

1. Attempt to connect to Postgres using old password (This shouldn't work):
    ```
    kubectl run demo-psql-postgresql-client --rm  -it --restart='Never' --namespace $DEMO_NAMESPACE \
        --image docker.io/bitnami/postgresql:11.11.0-debian-10-r31 \
        --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host demo-psql-postgresql -U democlient -d postgres -p 5432
    ```

1. Save the password again:
    ```
    export POSTGRES_PASSWORD=$(kubectl exec -it vault-0 -n default -- vault kv get -format json secret/demo-config | jq ".data.data.password" -rj)
    ```

1. Attempt to connect to Postgres using new password (You might have to press enter, but you'll get a `postgres=>` prompt):
    ```
    kubectl run demo-psql-postgresql-client --rm  -it --restart='Never' --namespace $DEMO_NAMESPACE --image docker.io/bitnami/postgresql:11.11.0-debian-10-r31 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host demo-psql-postgresql -U democlient -d postgres -p 5432
    ```

### Cleanup
1. Delete namespace:
    ```
    kubectl delete namespace/$DEMO_NAMESPACE
    ```

1. Delete Vault secrets:
    ```
    kubectl exec -it vault-0 -n default -- vault kv delete secret/demo-config
    kubectl exec -it vault-0 -n default -- vault kv delete secret/demo-master
    ```

## Project Links

### Development

All of the development process takes place in this project:
* Issue, including new features - https://gitlab.com/darkwolf-cc2/start-here/-/issues
* Pipelines - https://gitlab.com/darkwolf-cc2/start-here/-/pipelines
* Merge Requests - https://gitlab.com/darkwolf-cc2/start-here/-/merge_requests

### Side Projects

* [K2so](https://gitlab.com/darkwolf-cc2/ansible/k2so) - This is where primary Ansible development happens. Tasks that work here are pulled into this project (start-here) for further testing and integration. We decided to keep this out of tree so that our Ansible team could work faster on their own while the Operator team was bootstrapping.
* [cc_stateful_db_app](https://gitlab.com/darkwolf-cc2/cc_stateful_db_app) - This is an application that we made to demonstrate the database connection. It's more of a Python3 FastAPI demo than anything else, but we had the code mostly on hand, so it was quick to bring into the project.

### Communications

* All team communications go through the [Dark Wolf Enterprise Slack](https://darkwolf.slack.com/). The main channel is #cloud-citi-ext, with some ansible specific discussion taking place on #cloud-citi-ansible. To request permission to join, please reach out to Will

## Public Images

* Public container images are hosted on Quay.io. To get started with a public image `docker pull quay.io/will_k/start-here`

## Getting Set Up for Development

To allow individual developers to develop the operator independently, we recommend either configuring [Kubernetes in Docker](https://kind.sigs.k8s.io/) or [k3s](https://rancher.com/docs/k3s/latest/en/).\
Helm is required to install Vault and Postgreql instructions can be found at the [Helm Quickstart Guide](https://helm.sh/docs/intro/quickstart/)\
**In development mode, Vault will *forget all of your configuration* when it restarts.**

1. After setting up your local development cluster and cloning the project, you can start by deploying the operator from the latest public tag:
    ```
    $ kubectl apply -k config/default
    
        namespace/start-here-system created
        customresourcedefinition.apiextensions.k8s.io/rotations.encryption.darkwolfsolutions.com created
        role.rbac.authorization.k8s.io/start-here-leader-election-role created
        clusterrole.rbac.authorization.k8s.io/start-here-manager-role created
        clusterrole.rbac.authorization.k8s.io/start-here-metrics-reader created
        clusterrole.rbac.authorization.k8s.io/start-here-proxy-role created
        rolebinding.rbac.authorization.k8s.io/start-here-leader-election-rolebinding created
        clusterrolebinding.rbac.authorization.k8s.io/start-here-manager-rolebinding created
        clusterrolebinding.rbac.authorization.k8s.io/start-here-proxy-rolebinding created
        service/start-here-controller-manager-metrics-service created
        deployment.apps/start-here-controller-manager created
    ```

1. Deploy Vault in development mode using Helm:
    ```
    $ curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
    $ chmod 700 get_helm.sh
    $ ./get_helm.sh

        Downloading https://get.helm.sh/helm-v3.5.3-linux-amd64.tar.gz
        Verifying checksum... Done.
        Preparing to install helm into /usr/local/bin
        helm installed into /usr/local/bin/helm

    $ helm repo add hashicorp https://helm.releases.hashicorp.com

        "hashicorp" has been added to your repositories

    $ helm install vault hashicorp/vault --set "server.dev.enabled=true" --wait

        NAME: vault
        LAST DEPLOYED: Thu Mar 25 14:52:03 2021
        NAMESPACE: default
        STATUS: deployed
        REVISION: 1
        TEST SUITE: None
        NOTES:
        Thank you for installing HashiCorp Vault!
        ...
    ```

1. Wait for the Vault pods to come up
    ```
    $ kubectl get pods

        NAME                                    READY   STATUS    RESTARTS   AGE
        vault-0                                 1/1     Running   0          80s
        vault-agent-injector-784ccfc788-nlsr4   1/1     Running   0          80s
    ```

1. Deploy Postgres using Helm:
    ```
    $ helm repo add bitnami https://charts.bitnami.com/bitnami
    
        "bitnami" has been added to your repositories

    $ helm install development bitnami/postgresql --wait

        NAME: development
        LAST DEPLOYED: Thu Mar 25 14:53:40 2021
        NAMESPACE: default
        STATUS: deployed
        REVISION: 1
        TEST SUITE: None
        NOTES:
        ** Please be patient while the chart is being deployed **

        PostgreSQL can be accessed via port 5432 on the following DNS name from within your cluster:

            development-postgresql.default.svc.cluster.local - Read/Write connection
        ...
    ```

1. Get the postgres default account password and store it in your first secret
    ```
    $ export POSTGRES_PASSWORD=$(kubectl get secret --namespace default development-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)
    $ kubectl exec -it vault-0  -- vault kv put secret/dev-master username=postgres password=${POSTGRES_PASSWORD} db_host=development-postgresql.default.svc.cluster.local
        Key              Value
        ---              -----
        created_time     2021-03-25T14:54:49.717061145Z
        deletion_time    n/a
        destroyed        false
        version          1
    ```

1. Use that password to create a database on Postgresql:
    ```
    $ kubectl run development-postgresql-client --rm  -it --restart='Never' --namespace default --image docker.io/bitnami/postgresql:11.11.0-debian-10-r31 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host development-postgresql -U postgres -d postgres -p 5432 -c "CREATE DATABASE dev;"
    
        CREATE DATABASE
        pod "development-postgresql-client" deleted
    ```

1. Add a client configuration to our Vault:
    ```
    $ kubectl exec -it vault-0 -- vault kv put secret/dev-config username=client password="SuperSecret" db_name=dev db_host=development-postgresql.default.svc.cluster.local

        Key              Value
        ---              -----
        created_time     2021-03-25T15:03:40.117168252Z
        deletion_time    n/a
        destroyed        false
        version          1
    ```


1. Enable Kubernetes authentication so clients can authenticate with Kubernetes Service Accounts and configure the Kubernetes authentication method to use the service account token, the location of the Kubernetes host, and its certificate. These commands are easier to execute inside the context of the vault pod. Open a shell on the Vault pod `kubectl exec -it vault-0 -- /bin/sh` and run the following commands:
    ```
    / $ vault auth enable kubernetes

        Success! Enabled kubernetes auth method at: kubernetes/

    / $ vault secrets enable -path=internal kv-v2

        Success! Enabled the kv-v2 secrets engine at: internal/

    / $ vault write auth/kubernetes/config \
        token_reviewer_jwt="$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)" \
        kubernetes_host="https://$KUBERNETES_PORT_443_TCP_ADDR:443" \
        kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt

            Success! Data written to: auth/kubernetes/config

    / $ cat <<EOF | vault policy write start-here-operator -
    path "secret/*" {
        capabilities = ["create", "read", "update", "delete", "list", "sudo"]
    }
    EOF

            Success! Uploaded policy: start-here-operator

    / $ vault write auth/kubernetes/role/start-here \
        bound_service_account_names=default \
        bound_service_account_namespaces=start-here-system \
        policies=start-here-operator,default \
        ttl=24h
    
            Success! Data written to: auth/kubernetes/role/start-here

    / $ exit
    ```

1. Map the new role to the Kubernetes service account:
    ```
    $ kubectl exec -it vault-0 -- vault write auth/kubernetes/role/start-here \
        bound_service_account_names=default \
        bound_service_account_namespaces=start-here-system \
        policies=start-here-operator \
        ttl=24h

            Success! Data written to: auth/kubernetes/role/start-here

    ```


1. Grant Vault RBAC permissions:
    ```
    $ cat <<EOF | kubectl apply -f -
    apiVersion: rbac.authorization.k8s.io/v1beta1
    kind: ClusterRoleBinding
    metadata:
        name: role-tokenreview-binding
        namespace: default
    roleRef:
        apiGroup: rbac.authorization.k8s.io
        kind: ClusterRole
        name: system:auth-delegator
    subjects:
        - kind: ServiceAccount
          name: vault-auth
          namespace: default
    EOF
    
            Warning: rbac.authorization.k8s.io/v1beta1 ClusterRoleBinding is deprecated in v1.17+, unavailable in v1.22+; use rbac.authorization.k8s.io/v1 ClusterRoleBinding
            clusterrolebinding.rbac.authorization.k8s.io/role-tokenreview-binding created
    ```

1. Submit our first rotation resource to initialize the database client user:
    ```
    $ cat <<EOF | kubectl apply -f -
    apiVersion: encryption.darkwolfsolutions.com/v1
    kind: Rotation
    metadata:
        name: rotation-1
    spec:
        client_config: secret/dev-config
        master_config: secret/dev-master
        vault_url: "http://vault-internal.default.svc.cluster.local:8200"
    EOF

            rotation.encryption.darkwolfsolutions.com/rotation-1 created
    ```

1. Confirm that the dev-config secret was rotated:
    ```
    $ kubectl exec -it vault-0 -- vault kv get secret/dev-config

        ====== Metadata ======
        Key              Value
        ---              -----
        created_time     2021-03-25T15:08:30.578388213Z
        deletion_time    n/a
        destroyed        false
        version          6

        ====== Data ======
        Key         Value
        ---         -----
        db_host     development-postgresql.default.svc.cluster.local
        db_name     dev
        password    MRwUIOnWbhK5KIon
        username    client
    ```

1. Last, confirm that the new client password works (You may need to install `jq` for this command) (type `exit` to exit psql shell):
    ```
    $ export POSTGRES_PASSWORD=$(kubectl exec -it vault-0 -- vault kv get -format json secret/dev-config | jq ".data.data.password" -rj)
    $ kubectl run development-postgresql-client --rm  -it --restart='Never' --namespace default --image docker.io/bitnami/postgresql:11.11.0-debian-10-r31 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host development-postgresql -U client -d postgres -p 5432

            If you don't see a command prompt, try pressing enter.

            postgres=> \l
            postgres=> exit
            pod "development-postgresql-client" deleted
    ```

1. After confirming access, here are some useful commands:
    * `buildah bud . -t controller:dev` - build the container with buildah
    * `sudo docker build . -t controller:dev` build the container with docker
    * `kubectl apply -k config/default` - update the images in your cluster. Update the newImage/newTag parameters in the config/manager/kustomization.yaml file to match your new image (controller:dev above)
    * `kubectl logs -f -n start-here-system deployment/start-here-controller-manager manager` - view the recent ansible logs from the operator


## Testing From Local Machine

- Two testing mode/infrastructure are supported. Using the [Molecule](https://molecule.readthedocs.io/en/latest/usage.html) framework.
- Prerequisites for both: 
  - Need tools listed at: [Ansible Based Operator Testing with Molecule](https://sdk.operatorframework.io/docs/building-operators/ansible/testing-guide/)
  - Configure image location: `export OPERATOR_IMAGE=registry.gitlab.com/darkwolf-cc2/start-here/rotation:v1.0.0`
- Mode on [Kubernetes in Docker](https://kind.sigs.k8s.io/): 
   - Command: `molecule test --scenario-name kind`
- Mode on `Existing Kubernetes`:
   - Prerequisites:
       - Home Folder: `.kube/config`
       - Need `jq` and `kubectl` on a Linux shell
   - Command: `molecule test`


## Testing From the CI/CD Pipeline of GitLab

   * [Branch `gitlab-runner`](https://gitlab.com/darkwolf-cc2/gitlab-runner) contains the Terraform script to create a GitLab Runner configured to be used for the Operator's automated tests.

